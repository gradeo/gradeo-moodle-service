FROM openjdk:11.0.7-jre-slim-buster
COPY build/libs/Gradeo_Moodle_service-*-all.jar Gradeo_Moodle_service.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "Gradeo_Moodle_service.jar"]